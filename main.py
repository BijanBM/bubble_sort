# this is a program to solve the game bubble sort
# it is has nothing to do with the sorting algorithm

# the user enters the problem, and this program outputs a solution
# every colour is represented with a string
# the tubes are entered one per line from bottom to top

# first if finds the solution using depth first search
# then it deletes redundant steps from the solution

from copy import deepcopy
from sys import setrecursionlimit
setrecursionlimit(10000) # some of the levels will exceed the default limit
# this may need to be raised higher as I have not tested every level

visited = set() # this set prevents backtracking

def into_tuple(l):
	# this is required because lists are not hashable
	return tuple([tuple(x) for x in l])

def solve(state):
	# takes a state as input
	# the state is a list of tubes, where each tube is represented as a list
	# each ball is a string, although the type does not matter particularly

	# returns a sequence of moves if it is solvable
	# a move is a tuple containing the startpoint and endpoint, both as indices
	# returns False otherwise
	if is_solved(state):
		return [] # if it is a solved state no moves are required
	t = into_tuple(state) 
	if t in visited:
		return False # although it returns false it might technically be solvable
	visited.add(t) # this marks the state as visited
	for i in range(len(state)):
		for j in range(len(state)):
			# these 2 loops try every move, i is the source, j is the destination
			if i==j:
				# you can't move a ball to its current location
				continue
			if state[i] and (not state[j] or len(state[j])<4 and state[i][-1]==state[j][-1]):
				# the boolean expression above check if the move is valid
				# the state is not duplicated, instead it makes an operation,
				# and undoes it if it is unsuccessful
				state[j].append(state[i].pop())
				s = solve(state)
				state[i].append(state[j].pop())
				if isinstance(s, list):
					return [(i,j)]+s
	return False

def is_solved(state):
	# this determines if it has reached a solved state
	# first it checks that every tube with 2 or more balls is homogeneous
	# then it checks that no two tubes have the same ball
	# if it finds either it returns False, otherwies True
	for tube in state:
		if len(tube)>=2:
			for i in tube[1:]:
				if i!=tube[0]:
					return False
	for i in range(len(state)):
		if not state[i]:
			continue
		for j in range(len(state)):
			if i == j:
				continue
			if not state[j]:
				continue
			if state[i][0] == state[j][0]:
				return False
			
	return True

def read_state():
	# returns the state as specified in stdin
	# has no error handling so input must be formatted correctly
	state = []
	while True:
		# each line of input is either a tube or 'exit'
		# exit means the input is finished
		# a tube is a sequence of whitespace seperated symbols that represent the colour
		# the balls are to be read bottom to top
		# an empty line is an empty tube
		in_str = input()
		if in_str == 'exit':
			return state
		state.append(in_str.split())


def optimize_solution(moves, state):
	# performs three rudimetary simplifications

	# if a ball is moved, and then moved back, both moves are deleted

	# if a ball is moved, and then immidiately moved again, the 2 moves are combined 

	# the third case is subtle because it effecively handles 2 different problems
	# the first is that if every ball is moved from 1 tube to an empty tube, these moves are deleted
	# the second is that if every ball is moved from 1 tube to a tube with a single ball,
	# and atleast 2 balls are moved, then instead the one ball can be moved in the reverse direction
	# in either case, the 2 are swapped
	for _ in range(16):
		# they're all done 16 times because sometimes optimizations cascade off one another

		# this loop performs the first optimization
		# it must be a while loop because the length is changing
		i = 0
		while i < len(moves):
			for j in range(i+1, len(moves)):
				# this inner loop looks for the next instruction that shares an endpoint with i
				# if it's simply i in reverse both are deleted
				if moves[i][0] in moves[j] or moves[i][1] in moves[j]:
					if moves[j] == (moves[i][1], moves[i][0]):
						moves.pop(j)
						moves.pop(i)
						i -= 1	
					break
			i += 1	

		# this loop performs the second optimization
		i = 0
		while i < len(moves)-1:
			if moves[i][1] == moves[i+1][0]:
				combined = (moves.pop(i)[0], moves.pop(i)[1])
				# the insertion is conditional because the combined move may not be needed at all
				if combined[0] != combined[1]:
					moves.insert(i, combined)						
			i+=1

		# this loop performs the third optimization
		i = 0
		new_state = deepcopy(state) # this is so as not to mutate the state parameter
		while i < len(moves):
			deleted = False # this boolean tracks if a deletion was made
			src = moves[i][0]
			dst = moves[i][1]
			if len(new_state[dst]) <= 1:
				# if a ball is being moved to tube of height atmost 1, the subsequent moves are checked
				identical = [i] # identical is the places where the same move is made
				# it stops after a move distinct from i that shares an endpoint
				for j in range(i+1, len(moves)):
					# this loop collects up the values of identical
					if moves[j] == moves[i]:
						identical.append(j)
					elif moves[i][0] in moves[j] or moves[i][1] in moves[j]:
						break
				if len(new_state[moves[i][0]])==len(identical) and len(new_state[moves[i][1]])<len(identical):
					# this condition checks if infact all the balls are being moved
					deleted = True
					# this loops deletes the operations
					# it must do so backwards
					for j in identical[::-1]:
						moves.pop(j)
					# this loop performs all the necessary swaps
					for j in range(i, len(moves)):
						if moves[j][0] == src:
							moves[j] = (dst, moves[j][1])
						elif moves[j][0] == dst:
							moves[j] = (src, moves[j][1])
						if moves[j][1] == src:
							moves[j] = (moves[j][0], dst)
						elif moves[j][1] == dst:
							moves[j] = (moves[j][0], src)
					if len(new_state[dst])==1:
						moves.insert(i, (dst, src))
			if not deleted:
				# if a move is deleted, a new move will be shifted into its place
				# this new move will not have been vetted yet, and should not be performed
				new_state[moves[i][1]].append(new_state[moves[i][0]].pop())
				i += 1
	return moves

def main():
	state = read_state()
	solution = optimize_solution(solve(state), state)
	for move in solution:
		print(move)

if __name__ == '__main__':
	main()
